


import svgwrite
import sys

print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))

dwg = svgwrite.Drawing('output.svg', profile='tiny')


# draw a red box
dwg.add(dwg.rect((10, 10), (600, 400),
    stroke=svgwrite.rgb(10, 10, 16, '%'),
    fill='red')
)

line =125
for arg in sys.argv:
    # Draw some text demonstrating font_size, font_family, font_weight, font_color
    if arg!='main.py':
        print('Gerando',arg);
        dwg.add(dwg.text(arg,
            insert=(55,line),
            stroke='none',
            fill='#900',
            font_size='90px',
            font_weight="bold",
            font_family="Arial")
        )
        line = line+90

# output our svg image as raw xml
print('Gerado output.svg..............')
print(dwg.tostring())
# write svg file to disk
dwg.save()
